<?php

namespace TMWK\ExceptionNotifierBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridInterface;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollectionInterface;
use Sonata\AdminBundle\Show\ShowMapper;

final class ExceptionNotifierAdmin extends AbstractAdmin
{
//    use SortableAdminTrait;
    protected $baseRouteName    = 'exception_notifier';
    protected $baseRoutePattern = 'exception_notifier';

    protected function configureRoutes(RouteCollectionInterface $collection): void
    {
        $collection->remove('create');
        $collection->remove('edit');
    }

    protected function configureDefaultSortValues(array &$sortValues): void
    {
        $sortValues[DatagridInterface::PAGE]       = 1;
        $sortValues[DatagridInterface::SORT_ORDER] = 'DESC';
        $sortValues[DatagridInterface::SORT_BY]    = 'id';
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            ->add('message', null, ['label' => 'Tipo']);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('message', null, ['label' => 'Tipo']);
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->addIdentifier('id', null, array('label' => '#', 'header_style' => 'width: 1%;'))
            ->add('message', null, ['label' => 'Tipo'])
            ->add('create_at', null, ['label' => 'Fecha'])
            ->add(ListMapper::NAME_ACTIONS, ListMapper::TYPE_ACTIONS, [
                'translation_domain' => 'SonataAdminBundle',
                'actions'            => [
                    'show' => [],
                    //                    'edit' => [],
                    /*'move' => [
                        'template' => '@RunroomSortableBehavior/sort.html.twig',
                        'enable_top_bottom_buttons' => false,
                    ],*/
                ],
            ]);
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->add('data_method', null, ['label' => 'Método'])
            ->add('message', null, ['label' => 'Tipo'])
            ->add('file', null, ['label' => 'Archivo'])
            ->add('line', null, ['label' => 'Linea'])
            ->add('data_request', null, ['label' => 'Parámetros POST', 'template' => "@TMWKExceptionNotifier/sonata/deserialize.html.twig"])
            ->add('data_query', null, ['label' => 'Parámetros GET', 'template' => "@TMWKExceptionNotifier/sonata/deserialize.html.twig"])
            ->add('data_session', null, ['label' => 'Parámetros Sesiones', 'template' => "@TMWKExceptionNotifier/sonata/deserialize.html.twig"])
            ->add('create_at', null, ['label' => 'Fecha']);
    }

}