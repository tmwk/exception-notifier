<?php
/*
 * ************************************************************************
 *  * Nombre del Archivo: RedirectListener.php
 *  * Autor: Mario Figueroa [mfigueroa@tmwk.cl]
 *  * Fecha de Creación: 2/8/23 16:54
 *  ***********************************************************************
 *  * Copyright (c) 2023 Mario Figueroa
 *  * Queda prohibida la distribución y uso no autorizado de este archivo.
 *  * Para obtener más detalles, consulta el archivo LICENSE.md
 *  ***********************************************************************
 */

namespace TMWK\ExceptionNotifierBundle\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use TMWK\ExceptionNotifierBundle\Entity\ExceptionNotification;

final class ExceptionNotificationSubscriber implements EventSubscriberInterface
{
    private EntityManagerInterface $entityManager;
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => [
                ['registerException', 10],
            ],
        ];
    }

    public function registerException(ExceptionEvent $event): RedirectResponse|false
    {
        $entity = new ExceptionNotification();

        $entity->setDataSession(serialize($event->getRequest()->getSession()->all()));
        $entity->setDataQuery(serialize($event->getRequest()->query->all()));
        $entity->setDataRequest(serialize($event->getRequest()->request->all()));
        $entity->setDataMethod($event->getRequest()->getMethod());

        $exception = $event->getThrowable();

        $entity->setMessage($exception->getMessage());
        $entity->setCode($exception->getCode());
        $entity->setFile($exception->getFile());
        $entity->setLine($exception->getLine());

        if ($exception instanceof HttpExceptionInterface) {
            if ($exception->getStatusCode() == 404) {
                $entity->setStatusCode($exception->getStatusCode());
            }
        }

        $this->entityManager->persist($entity);
        $this->entityManager->flush();
        return false;
    }
}