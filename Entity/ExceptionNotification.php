<?php
/*
 * ************************************************************************
 *  * Nombre del Archivo: ExceptionNotification.php
 *  * Autor: Mario Figueroa [mfigueroa@tmwk.cl]
 *  * Fecha de Creación: 7/8/23 15:06
 *  ***********************************************************************
 *  * Copyright (c) 2023 Mario Figueroa
 *  * Queda prohibida la distribución y uso no autorizado de este archivo.
 *  * Para obtener más detalles, consulta el archivo LICENSE.md
 *  ***********************************************************************
 */

namespace TMWK\ExceptionNotifierBundle\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
//use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="exception_notification")
 */
class ExceptionNotification
{
    /**
    * @ORM\Id
    * @ORM\Column(type="integer")
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $id;

    /**
    * @var string $message
    *
    * @ORM\Column(type="text", nullable=true)
    */
    protected $message;

    /**
    * @var string $file
    *
    * @ORM\Column(type="string", nullable=true)
    */
    protected $file;

    /**
    * @var integer $line
    *
    * @ORM\Column(type="integer", nullable=true)
    */
    protected $line;

    /**
    * @var string $code
    *
    * @ORM\Column(type="string", nullable=true)
    */
    protected $code;

    /**
    * @var integer $status_code
    *
    * @ORM\Column(type="integer", nullable=true, length=3)
    */
    protected $status_code;
    
    /**
    * @var string $data_request
    *
    * @ORM\Column(type="text", nullable=true)
    */
    protected $data_request;

    /**
    * @var string $data_query
    *
    * @ORM\Column(type="text", nullable=true)
    */
    protected $data_query;

    /**
    * @var string $data_session
    *
    * @ORM\Column(type="text", nullable=true)
    */
    protected $data_session;

    /**
    * @var string $data_method
    *
    * @ORM\Column(type="string", length=4, nullable=true)
    */
    protected $data_method;

    /**
    * @var string $create_at
    *
    * @Gedmo\Timestampable(on="create")
    * @ORM\Column(type="datetime", nullable=true)
    */
    protected $create_at;

    public function __toString(): string
    {
        return $this->message;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(?string $message): static
    {
        $this->message = $message;

        return $this;
    }

    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(?string $file): static
    {
        $this->file = $file;

        return $this;
    }

    public function getLine(): ?int
    {
        return $this->line;
    }

    public function setLine(?int $line): static
    {
        $this->line = $line;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): static
    {
        $this->code = $code;

        return $this;
    }

    public function getStatusCode(): ?int
    {
        return $this->status_code;
    }

    public function setStatusCode(?int $status_code): static
    {
        $this->status_code = $status_code;

        return $this;
    }

    public function getDataRequest(): ?string
    {
        return $this->data_request;
    }

    public function setDataRequest(?string $data_request): static
    {
        $this->data_request = $data_request;

        return $this;
    }

    public function getDataQuery(): ?string
    {
        return $this->data_query;
    }

    public function setDataQuery(?string $data_query): static
    {
        $this->data_query = $data_query;

        return $this;
    }

    public function getDataSession(): ?string
    {
        return $this->data_session;
    }

    public function setDataSession(?string $data_session): static
    {
        $this->data_session = $data_session;

        return $this;
    }

    public function getDataMethod(): ?string
    {
        return $this->data_method;
    }

    public function setDataMethod(?string $data_method): static
    {
        $this->data_method = $data_method;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->create_at;
    }

    public function setCreateAt(?\DateTimeInterface $create_at): static
    {
        $this->create_at = $create_at;

        return $this;
    }

}